﻿using CitiesAndContries.Clases;
using CitiesAndContries.Entities;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CitiesAndContries
{
    class Program
    {
        static string Current => Directory.GetCurrentDirectory();

        static async Task Main(string[] args)
        {
            // country();
            // States();
            Cities();
            Console.Read();
        }
        static string FileNameCountrySave = "Nivel I-Tabla de Paises-Process";
        static string FileNameStateSave = "Nivel II-Provincia-departamentos-estado existentes";
        static string FileNameCitiesSave = "Nivel III-Ciudad-municipio-capital existentes";
        static string OriginalFile = "OriginalFiles/Countries/Nivel I-Tabla de Paises.xlsx";
        static string OriginalFilestate = "OriginalFiles/Countries/Nivel II-Provincia-departamentos-estado existentes.xlsx";
        static string OriginalFilecities = "OriginalFiles/Countries/Nivel III-Ciudad-municipio-capital existentes.xlsx";
        static string Equivalencia = "OriginalFiles/Countries/EquivalenciasLetras.xlsx";

        static public List<ReemplaceDto> Equivalencias
        {
            get
            {
                return JsonConvert.DeserializeObject<List<ReemplaceDto>>(ExcelProcess.ReadExcel(Equivalencia));
            }
        }
        static public List<CountriesBaseDoc> OriginalFileCountries
        {
            get
            {
                return JsonConvert.DeserializeObject<List<CountriesBaseDoc>>(ExcelProcess.ReadExcel(OriginalFile));
            }
        }
        static public List<StatesBaseDoc> OriginalFileEstates
        {
            get
            {
                return JsonConvert.DeserializeObject<List<StatesBaseDoc>>(ExcelProcess.ReadExcel(OriginalFilestate));
            }
        }

        static public List<CitiesBaseDoc> OriginalFileCities
        {
            get
            {
                return JsonConvert.DeserializeObject<List<CitiesBaseDoc>>(ExcelProcess.ReadExcel(OriginalFilecities));
            }
        }

        private static void country()
        {
            var jsonfile = $"{Current}/json/{FileNameCountrySave.Replace(" ", "")}.json";
            var execlfile = $"{FileNameCountrySave}.xlsx";
            FileInfo json = new FileInfo(jsonfile);
            List<CountriesBaseDoc> basicc = null;
            if (!json.Exists)
            {
                Console.WriteLine("############### T R A D U C I E N D O  P A I S E S ##################");

                var countiesjson = LoadJson.LoadJsonFile($"{Current}/json/countries+states+cities.json");
                var conutrie = JsonConvert.DeserializeObject<CountriesDto>(countiesjson);
                basicc = conutrie.countries.Select(x => new CountriesBaseDoc()
                {
                    CODIGO_PAIS_ISO2 = x.iso2,
                    CODIGO_PAIS_ISO3 = x.iso3,
                    TRADUCCION_INGLES = x.name,
                    NOMBRE_DEL_PAIS = GetURI(x.name)?.Result.Outputs[0].Output
                }).ToList();

                LoadJson.WritteJson(jsonfile, JsonConvert.SerializeObject(basicc));
            }
            else
            {
                Console.WriteLine("############### C A R G A D O  P A I S E S ##################");

                basicc = JsonConvert.DeserializeObject<List<CountriesBaseDoc>>(LoadJson.LoadJsonFile(jsonfile));
            }
            Console.WriteLine("############### C A L C U L A N D O   S I M I L I T U D   P A I S E S ##################");

            foreach (var country in basicc)
            {
                // country.CODIGO_PAIS = OriginalFileCountries.FirstOrDefault(x => x.NOMBRE_DEL_PAIS != null && StringSimilarity.Calculate(x.NOMBRE_DEL_PAIS, country.NOMBRE_DEL_PAIS) > 75)?.CODIGO_PAIS;

                country.CODIGO_PAIS = OriginalFileCountries.FirstOrDefault(x => x.TRADUCCION_INGLES != null && StringSimilarity.Calculate(x.TRADUCCION_INGLES, country.TRADUCCION_INGLES) > 75)?.CODIGO_PAIS;
            }


            DataTable table = ConvertData(basicc);
            Console.WriteLine("############### P A I S E S   C R E A D O S ##################");

            ExcelProcess.WriteExcel(execlfile, table);

            Console.WriteLine(ExcelProcess.ReadExcel(execlfile));
            Console.WriteLine();
        }

        private static void States()
        {
            var jsonfile = $"{Current}/json/{FileNameStateSave.Replace(" ", "")}.json";
            var statetraslate = $"{Current}/json/statetraslate.json";

            var jsonfilecountry = $"{Current}/json/{FileNameCountrySave.Replace(" ", "")}.json";
            var countriesprocess = OriginalFileCountries;
            var execlfile = $"{FileNameStateSave}.xlsx";
            FileInfo json = new FileInfo(jsonfile);
            List<StatesBaseDoc> basicc = null;
            if (!json.Exists)
            {
                Console.WriteLine("############### T R A D U C I E N D O  E  S T A D O S ##################");

                var statesdata = LoadJson.LoadJsonFile($"{Current}/json/states.json");
                var items = JsonConvert.DeserializeObject<StatesDto>(statesdata);

                var statetraslateobj = JsonConvert.DeserializeObject<List<StatesBaseDoc>>(LoadJson.LoadJsonFile(statetraslate));

                basicc = items.states.Select(x => new StatesBaseDoc()
                {
                    // NOMBRE_PROVINCIA = statetraslateobj.SingleOrDefault(g => g.TRADUCCION_INGLES==x.name &&  g.CODIGO_PROVINCIA_ISO3 == x.state_code).NOMBRE_PROVINCIA,

                    CODIGO_PAIS = countriesprocess.SingleOrDefault(f => f.CODIGO_PAIS_ISO2 == x.country_code)?.CODIGO_PAIS,
                    CODIGO_PROVINCIA_ISO3 = x.state_code,
                    CODIGO_PAIS_ISO2 = x.country_code,
                    NOMBRE_PROVINCIA = GetURI(x.name)?.Result.Outputs[0].Output,
                    TRADUCCION_INGLES = x.name,
                    NOMBRE_DEL_PAIS = countriesprocess.SingleOrDefault(f => f.CODIGO_PAIS_ISO2 == x.country_code)?.NOMBRE_DEL_PAIS
                }
                ).ToList();

                LoadJson.WritteJson(jsonfile, JsonConvert.SerializeObject(basicc));
            }
            else
            {
                Console.WriteLine("# # # # # # # # # # # # # # #  C A R G A D O  E  S T A D O S  # # # # # # # # # # # # # # # # ##");

                basicc = JsonConvert.DeserializeObject<List<StatesBaseDoc>>(LoadJson.LoadJsonFile(jsonfile));
            }
            Console.WriteLine("############### C A L C U L A N D O   S I M I L I T U D   E  S T A D O S ##################");
            int ac = 0;
            //foreach (var state in basicc)
            foreach (var state in basicc)
            {

                state.CODIGO_PROVINCIA = OriginalFileEstates.FirstOrDefault(x => x.NOMBRE_PROVINCIA != null && StringSimilarity.Calculate(x.NOMBRE_PROVINCIA, state.NOMBRE_PROVINCIA) > 75)?.CODIGO_PROVINCIA;
                ac++;
                Console.WriteLine($"procesando {ac} de {basicc.Count()}");
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                ClearCurrentConsoleLine();
            }


            DataTable table = ConvertData(basicc);
            Console.WriteLine("############### E S T A D O S  C R E A D O S ##################");

            ExcelProcess.WriteExcel(execlfile, table);

            Console.WriteLine(ExcelProcess.ReadExcel(execlfile));
            Console.WriteLine();
        }
        static int acc = 0;
        static CitiesBaseDoc Setcities(CityDto x)
        {
            var country = OriginalFileCountries.SingleOrDefault(f => f.CODIGO_PAIS_ISO2 == x.country_code);
            var estate = OriginalFileEstates.SingleOrDefault(f => f.CODIGO_PAIS_ISO2 == x.country_code && f.CODIGO_PROVINCIA_ISO3 == x.state_code);

            var result = new CitiesBaseDoc()
            {
                CODIGO_PAIS = country?.CODIGO_PAIS,
                CODIGO_PROVINCIA_ISO3 = x.state_code,
                CODIGO_PAIS_ISO2 = x.country_code,
                NOMBRE_PROVINCIA = estate?.NOMBRE_PROVINCIA,
                CODIGO_CIUDAD_ISO = x.id,
                CODIGO_PROVINCIA = estate?.CODIGO_PROVINCIA,
                NOMBRE_CIUDAD = x.name,
                TRADUCCION_INGLES = x.name,
                NOMBRE_DEL_PAIS = country?.NOMBRE_DEL_PAIS,

            };


            try
            {
                Equivalencias.ForEach(i => result.NOMBRE_CIUDAD = result.NOMBRE_CIUDAD.Replace(i.Leter, (i.Reemplace != null ? i.Reemplace : "")));
            }
            catch
            {
                Console.WriteLine($"ERROR ");

            }
            try
            {

                Equivalencias.ForEach(i => result.NOMBRE_DEL_PAIS = result.NOMBRE_DEL_PAIS.Replace(i.Leter, (i.Reemplace != null ? i.Reemplace : "")));
            }

            catch
            {
                Console.WriteLine($"ERROR ");
            }

            try
            {
                Equivalencias.ForEach(i => result.NOMBRE_PROVINCIA = result.NOMBRE_PROVINCIA.Replace(i.Leter, (i.Reemplace != null ? i.Reemplace : "")));

            }
            catch {
                Console.WriteLine($"ERROR ");

            }

            try
            {

                result.CODIGO_CIUDAD = OriginalFileCities.FirstOrDefault(j => j.NOMBRE_CIUDAD != null && StringSimilarity.Calculate(j.NOMBRE_CIUDAD, x.name) > 70)?.CODIGO_CIUDAD;

            }
            catch
            {
                Console.WriteLine($"ERROR codigo ciudad ");


            }
            acc++;
            Console.WriteLine($"procesando { result.NOMBRE_DEL_PAIS } ciudad { result.NOMBRE_CIUDAD}  Numero {acc}");

            //  ClearCurrentConsoleLine();
            return result;
        }

        private static void Cities()
        {
            var jsonfile = @$"{Current}\json\{FileNameCitiesSave.Replace(" ", "")}.json";

            var jsonfilecountry = @$"{Current}\json\{FileNameCountrySave.Replace(" ", "")}.json";
            var countriesprocess = OriginalFileCountries;
            var execlfile = $"{FileNameCitiesSave}.xlsx";
            FileInfo json = new FileInfo(jsonfile);
            List<CitiesBaseDoc> basicc = null;
            if (!json.Exists)
            {
                Console.WriteLine("############### T R A D U C I E N D O  C I U D A D E S ##################");

                var cities = LoadJson.LoadJsonFile(@$"{Current}\json\cities.json");
                var items = JsonConvert.DeserializeObject<CitiesDto>(cities);
                //  var es = items.Cities.Take(4);
                basicc = items.Cities.Select(x => Setcities(x)).ToList();

                LoadJson.WritteJson(jsonfile, JsonConvert.SerializeObject(basicc));
            }
            else
            {
                Console.WriteLine("# # # # # # # # # # # # # # #  C A R G A D O   C I U D A D E S  # # # # # # # # # # # # # # # # ##");

                basicc = JsonConvert.DeserializeObject<List<CitiesBaseDoc>>(LoadJson.LoadJsonFile(jsonfile));
            }
            Console.WriteLine("############### C A L C U L A N D O   S I M I L I T U D    C I U D A D E S  ##################");
            int ac = 0;
            //foreach (var state in basicc)
            //foreach (var city in basicc)
            //{

            //    //city.CODIGO_CIUDAD = OriginalFileCities.FirstOrDefault(x => x.NOMBRE_CIUDAD != null && StringSimilarity.Calculate(x.NOMBRE_CIUDAD, city.NOMBRE_CIUDAD) > 70)?.CODIGO_PROVINCIA;




            //}


            DataTable table = ConvertData(basicc);
            Console.WriteLine("###############  C I U D A D E S  C R E A D O S ##################");

            ExcelProcess.WriteExcel(execlfile, table);

            Console.WriteLine(ExcelProcess.ReadExcel(execlfile));
            Console.WriteLine();
        }

        public static void ClearCurrentConsoleLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
        static async Task<OutputsDto> GetURI(string trasnlation)
        {
            try
            {
                Uri u = new Uri($"https://systran-systran-platform-for-language-processing-v1.p.rapidapi.com/translation/text/translate?source=en&target=es&input={trasnlation}");
                var response = string.Empty;
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("x-rapidapi-host", "systran-systran-platform-for-language-processing-v1.p.rapidapi.com");
                    client.DefaultRequestHeaders.Add("x-rapidapi-key", "e7854cb498mshf1799852356d18dp15a070jsne58cd1d62f53");

                    HttpResponseMessage result = await client.GetAsync(u);
                    if (result.IsSuccessStatusCode)
                    {
                        response = await result.Content.ReadAsStringAsync();
                    }
                }
                var resultf = JsonConvert.DeserializeObject<OutputsDto>(response);

                var originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write($"Inglés= {trasnlation} ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($" Español= {resultf.Outputs[0].Output}");
                Console.ForegroundColor = originalColor;
                Console.WriteLine("");
                return resultf;

            }
            catch
            {
                var Outputs = new List<OutputDto>();
                Outputs.Add(new OutputDto() { Output = "Error" });
                return new OutputsDto() { Outputs = Outputs };

            }
        }

        private static DataTable ConvertData<T>(List<T> lts)
        {
            DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(lts), (typeof(DataTable)));
            return table;
        }
    }
}
