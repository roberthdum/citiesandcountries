﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace CitiesAndContries
{
    public class LoadJson
    {
        public static string LoadJsonFile(string jsonFileName)
        {
            using (var r = new StreamReader(jsonFileName))
            {
                var json = r.ReadToEnd();
                return (json);
            }
        }

        public static void WritteJson(string filame, string json)
        {
            System.IO.File.WriteAllText(filame, json);
        }
    }
}
