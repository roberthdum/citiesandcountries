﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitiesAndContries.Entities
{
    public class CitiesDto
    {
        public List<CityDto> Cities { get; set; }
    }
}
