﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace CitiesAndContries.Entities
{
    public class OutputDto
    {
        [JsonPropertyName("output")]

        public string Output { get; set; }

    }
}
