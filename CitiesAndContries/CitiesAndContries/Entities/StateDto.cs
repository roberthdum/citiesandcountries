﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitiesAndContries.Entities
{
    public class StateDto
    {
        public string name
        { get; set; }
        public string country_code
        { get; set; }
        public string state_code
        { get; set; }
    
    }
}
