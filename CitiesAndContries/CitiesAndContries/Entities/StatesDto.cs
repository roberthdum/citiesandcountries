﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitiesAndContries.Entities
{
    public class StatesDto
    {
        public List<StateDto> states { get; set; }
    }
}
