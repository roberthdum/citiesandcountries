﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CitiesAndContries.Entities
{
    public class StatesBaseDoc
    {
        public string CODIGO_PAIS { get; set; }
        public string CODIGO_PAIS_ISO2 { get; set; }

        public string CODIGO_PROVINCIA_ISO3
        { get; set; }
        public string CODIGO_PROVINCIA
        { get; set; }
        public string NOMBRE_DEL_PAIS
        { get; set; }
        public string NOMBRE_PROVINCIA
        { get; set; }

        public string TRADUCCION_INGLES
        { get; set; }
    }
}
