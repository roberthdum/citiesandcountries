﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace CitiesAndContries.Entities
{
   public class OutputsDto
    {

        [JsonPropertyName("outputs")]

        public List<OutputDto> Outputs { get; set; }

    }
}
